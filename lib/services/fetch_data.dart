import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/list_chat_model.dart';

class FetchData {
  String url = 'https://5wzgz.mocklab.io/listchat';

  List<ListChatModel> listChat = [];

  static List<ListChatModel> parseAgents(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed
        .map<ListChatModel>((json) => ListChatModel.fromJson(json))
        .toList();
  }

  Future<List<ListChatModel>> fetchListChat() async {
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      List<ListChatModel> list = parseAgents(response.body);
      return list;
    } else {
      throw Exception('Failed to load post');
    }
  }
}
