import 'package:context_menus/context_menus.dart';
import 'package:desktop_app/views/home_page.dart';
import 'package:desktop_app/views/root_page.dart';
import 'package:flutter/material.dart';
import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:window_manager/window_manager.dart';
import 'menu/detail.dart';

void main() {
  runApp(const MyApp());
  windowManager.waitUntilReadyToShow().then((_) async {
    // await windowManager.setAsFrameless();
    // await windowManager.setTitleBarStyle(TitleBarStyle.hidden);
  });
  doWhenWindowReady(() {
    final win = appWindow;
    const initialSize = Size(300, 500);
    win.minSize = initialSize;
    win.size = initialSize;
    win.alignment = Alignment.center;
    win.show();
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // home: RootPage(),
      home: ContextMenuOverlay(child: const HomePage()),
    );
  }
}
