import 'package:desktop_app/models/list_chat_model.dart';
import 'package:flutter/material.dart';
import '../services/fetch_data.dart';
import 'package:context_menus/context_menus.dart';

class HomePageListChat extends StatefulWidget {
  const HomePageListChat({super.key});

  @override
  State<HomePageListChat> createState() => _HomePageListChatState();
}

class _HomePageListChatState extends State<HomePageListChat> {
  @override
  Widget build(BuildContext context) {
    final currentWidth = MediaQuery.of(context).size.width;
    return SizedBox(
      width: double.infinity,
      height: MediaQuery.of(context).size.height - 35,
      child: Row(
        children: [
          const Flexible(flex: 0, child: LeftBar()),
          const VerticalDivider(
            width: 1,
            color: Colors.grey,
          ),
          const Flexible(flex: 4, child: UserListChat()),
          currentWidth > 800
              ? const VerticalDivider(
                  width: 1,
                  color: Colors.grey,
                )
              : Container(),
          currentWidth > 800
              ? const Flexible(
                  flex: 7,
                  child: BoxChat(),
                )
              : Container(),
        ],
      ),
    );
  }
}

class LeftBar extends StatefulWidget {
  const LeftBar({super.key});

  @override
  State<LeftBar> createState() => _LeftBarState();
}

class _LeftBarState extends State<LeftBar> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height - 34,
      child: Column(
        children: [
          Container(
            child: leftBarItem(
              icon: Icons.message_outlined,
              onTap: () {},
            ),
          ),
          leftBarItem(icon: Icons.group_outlined, onTap: () {}),
          const Spacer(),
          // Setting Button
          InkWell(
            onTap: () {},
            hoverColor: Colors.grey[300],
            child: Container(
              padding: const EdgeInsets.all(4),
              child: settingButton(),
            ),
          ),
        ],
      ),
    );
  }

  Widget leftBarItem({required IconData icon, required Function() onTap}) {
    return InkWell(
      onTap: onTap,
      hoverColor: Colors.grey[300],
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
        child: Icon(
          icon,
          color: Colors.grey,
        ),
      ),
    );
  }

  Widget settingButton() {
    return PopupMenuButton(
      splashRadius: 1,
      offset: const Offset(0, -120),
      color: Colors.white,
      elevation: 3,
      position: PopupMenuPosition.over,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6),
      ),
      icon: const Icon(
        Icons.settings_outlined,
        color: Colors.grey,
      ),
      itemBuilder: (context) => [
        popupMenuItem(
          title: 'Language',
          trailingIcon: Icons.language,
        ),
        popupMenuItem(
          title: 'Log out',
          trailingIcon: Icons.logout,
        ),
      ],
    );
  }
}

class UserListChat extends StatefulWidget {
  const UserListChat({super.key});

  @override
  State<UserListChat> createState() => _UserListChatState();
}

class _UserListChatState extends State<UserListChat> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height - 34,
      child: SingleChildScrollView(
        child: Column(
          children: [
            searchBar(),
            const ListChat(),
          ],
        ),
      ),
    );
  }

  Widget searchBar() {
    return Container(
      margin: const EdgeInsets.all(12),
      height: 35,
      child: TextField(
        cursorColor: Colors.black,
        cursorWidth: 1,
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.only(right: 12),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide.none,
            ),
            prefixIcon: const Icon(
              Icons.search,
              color: Colors.black,
              size: 16,
            ),
            filled: true,
            fillColor: Colors.grey[200],
            hintText: 'Tìm kiếm',
            hintStyle: const TextStyle(fontSize: 14)),
      ),
    );
  }
}

class ListChat extends StatefulWidget {
  const ListChat({super.key});

  @override
  State<ListChat> createState() => _ListChatState();
}

class _ListChatState extends State<ListChat> {
  List<ListChatModel> listChat = [];

  @override
  void initState() {
    FetchData().fetchListChat().then((value) {
      setState(() {
        listChat = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ...List.generate(listChat.length, (index) {
          return ListChatItem(
            userName: listChat[index].userName,
            avatar: listChat[index].imageLink,
            recentMessage: listChat[index].recentMessage,
          );
        })
      ],
    );
  }
}

class ListChatItem extends StatelessWidget {
  final String userName;
  final String avatar;
  final String recentMessage;
  const ListChatItem(
      {super.key,
      required this.userName,
      required this.avatar,
      required this.recentMessage});

  @override
  Widget build(BuildContext context) {
    final currentWidth = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () {},
      child: Container(
        padding: const EdgeInsets.all(12),
        child: Row(
          children: [
            ContextMenuRegion(
              enableLongPress: false,
              contextMenu: GenericContextMenu(
                buttonStyle: const ContextMenuButtonStyle(
                  textStyle: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                  ),
                ),
                buttonConfigs: [
                  buttonConfigItem(
                    title: "Mark as read",
                    onPressed: () {},
                  ),
                  buttonConfigItem(
                    title: "Block",
                    onPressed: () {},
                  ),
                  buttonConfigItem(
                    title: "Delete",
                    onPressed: () {},
                  ),
                ],
              ),
              child: CircleAvatar(
                backgroundImage: NetworkImage(
                  avatar,
                ),
              ),
            ),
            const SizedBox(width: 12),
            SizedBox(
              width: currentWidth > 800
                  ? MediaQuery.of(context).size.width * 0.24
                  : MediaQuery.of(context).size.width * 0.55,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    userName,
                    style: const TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Text(
                    recentMessage,
                    style: const TextStyle(
                      fontSize: 14,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  ContextMenuButtonConfig buttonConfigItem({
    required String title,
    required Function() onPressed,
  }) {
    return ContextMenuButtonConfig(
      title,
      onPressed: onPressed,
    );
  }
}

class BoxChat extends StatelessWidget {
  const BoxChat({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        BoxChatTopBar(),
        Divider(
          height: 1,
          color: Colors.grey,
        ),
        Expanded(child: BoxChatMessage()),
        BoxChatBotBar(),
      ],
    );
  }
}

class BoxChatTopBar extends StatelessWidget {
  const BoxChatTopBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Row(
              children: const [
                SizedBox(
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(
                        'https://img.meta.com.vn/Data/image/2021/09/22/anh-meo-cute-de-thuong-dang-yeu-42.jpg'),
                  ),
                ),
                SizedBox(width: 12),
                Text(
                  'Cù Đình Thi',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {},
            hoverColor: Colors.grey[300],
            child: Padding(
              padding: const EdgeInsets.all(6.0),
              child: toolButton(),
            ),
          ),
        ],
      ),
    );
  }

  Widget toolButton() {
    return PopupMenuButton(
      splashRadius: 1,
      offset: const Offset(0, 45),
      color: Colors.white,
      elevation: 3,
      position: PopupMenuPosition.over,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6),
      ),
      icon: const Icon(
        Icons.more_horiz,
        color: Colors.grey,
      ),
      itemBuilder: (context) => [
        popupMenuItem(
          title: 'View Media',
          trailingIcon: Icons.filter,
        ),
        popupMenuItem(
          title: 'View File',
          trailingIcon: Icons.file_copy_outlined,
        ),
        popupMenuItem(
          title: 'Link',
          trailingIcon: Icons.link_outlined,
        ),
        popupMenuItem(
          title: 'Delete Conversation',
          trailingIcon: Icons.delete_outline_outlined,
        ),
      ],
    );
  }
}

class BoxChatMessage extends StatelessWidget {
  const BoxChatMessage({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: messages.length,
        itemBuilder: (context, index) => Container(
          padding:
              const EdgeInsets.only(left: 16, right: 16, top: 12, bottom: 12),
          child: Align(
            alignment: (messages[index].messageType == 'receiver'
                ? Alignment.topLeft
                : Alignment.topRight),
            child: ConstrainedBox(
              constraints: const BoxConstraints(
                maxWidth: 300,
              ),
              child: Column(
                crossAxisAlignment: messages[index].messageType == 'receiver'
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.end,
                children: [
                  // Username
                  Padding(
                    padding: messages[index].messageType == 'receiver'
                        ? const EdgeInsets.only(left: 12)
                        : const EdgeInsets.only(right: 12),
                    child: messages[index].messageType == 'receiver'
                        ? const Text(
                            'Cù Đình Thi',
                            style: TextStyle(color: Colors.black, fontSize: 12),
                          )
                        : const Text(''),
                  ),

                  // Message Content
                  Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                    decoration: BoxDecoration(
                        borderRadius: (messages[index].messageType == 'receiver'
                            ? const BorderRadius.only(
                                topRight: Radius.circular(20),
                                topLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20))
                            : const BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                                bottomLeft: Radius.circular(20))),
                        color: (messages[index].messageType == 'receiver'
                            ? Colors.grey[300]
                            : Colors.blue[300])),
                    child: Text(
                      messages[index].messageContent,
                      style: const TextStyle(fontSize: 15, color: Colors.black),
                    ),
                  ),

                  // Time message sent
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      '10:20',
                      style: TextStyle(fontSize: 12),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ChatMessage {
  String messageContent;
  String messageType;

  ChatMessage({required this.messageContent, required this.messageType});
}

List<ChatMessage> messages = [
  ChatMessage(messageContent: "Chào shop", messageType: "sender"),
  ChatMessage(
      messageContent: "Chào bạn, không biết mình có thể giúp gì cho bạn",
      messageType: "receiver"),
  ChatMessage(
      messageContent:
          "Không biết bên shop bạn có bạn bàn phím giả cơ không nhỉ?",
      messageType: "sender"),
  ChatMessage(messageContent: "Dạ bên mình có bạn", messageType: "receiver"),
  ChatMessage(
      messageContent:
          "Không biết bên shop bạn có bạn bàn phím giả cơ không nhỉ?",
      messageType: "sender"),
  ChatMessage(messageContent: "Dạ bên mình có bạn", messageType: "receiver"),
  ChatMessage(
      messageContent:
          "Không biết bên shop bạn có bạn bàn phím giả cơ không nhỉ?",
      messageType: "sender"),
  ChatMessage(messageContent: "Dạ bên mình có bạn", messageType: "receiver"),
];

class BoxChatBotBar extends StatelessWidget {
  const BoxChatBotBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        listTool(),
        Expanded(
          child: inputMessageField(),
        ),
        sendBtn()
      ],
    );
  }

  Widget inputMessageField() {
    return Container(
      margin: const EdgeInsets.only(right: 12, top: 12, bottom: 12),
      height: 35,
      child: TextField(
        cursorColor: Colors.black,
        cursorWidth: 1,
        style: const TextStyle(fontSize: 15),
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.only(right: 16, left: 16),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
              borderSide: BorderSide.none),
          fillColor: Colors.grey[200],
          filled: true,
        ),
      ),
    );
  }

  Widget sendBtn() {
    return Container(
      margin: const EdgeInsets.only(right: 12),
      child: InkWell(
        onTap: () {},
        borderRadius: BorderRadius.circular(20),
        hoverColor: Colors.grey[300],
        child: const Padding(
          padding: EdgeInsets.all(6.0),
          child: Icon(
            Icons.send_outlined,
            size: 20,
            color: Colors.blue,
          ),
        ),
      ),
    );
  }

  Widget listTool() {
    return Container(
      margin: const EdgeInsets.only(left: 12),
      child: Row(
        children: [
          listToolItem(icon: Icons.filter),
          listToolItem(icon: Icons.emoji_emotions_outlined),
          listToolItem(icon: Icons.attach_file)
        ],
      ),
    );
  }

  Widget listToolItem({required IconData icon}) {
    return Padding(
      padding: const EdgeInsets.only(right: 4),
      child: InkWell(
        onTap: () {},
        borderRadius: BorderRadius.circular(20),
        hoverColor: Colors.grey[300],
        child: Padding(
          padding: const EdgeInsets.all(6.0),
          child: Icon(
            icon,
            color: Colors.blue,
            size: 20,
          ),
        ),
      ),
    );
  }
}

PopupMenuItem popupMenuItem({
  required String title,
  required IconData trailingIcon,
}) {
  return PopupMenuItem(
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          trailingIcon,
          size: 16,
        ),
        const SizedBox(
          width: 12,
        ),
        Text(
          title,
          style: const TextStyle(
            color: Colors.black,
            fontSize: 15,
          ),
        ),
      ],
    ),
  );
}
