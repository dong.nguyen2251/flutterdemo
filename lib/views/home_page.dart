import 'dart:io';
import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:desktop_app/views/list_chat.dart';
import 'package:flutter/material.dart';
import 'package:system_tray/system_tray.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    initSystemTray();
    super.initState();
  }

  Future<void> initSystemTray() async {
    String path =
        Platform.isWindows ? 'assets/app_icon.ico' : 'assets/app_icon.png';

    final AppWindow appWindow = AppWindow();
    final SystemTray systemTray = SystemTray();

    // We first init the systray menu
    await systemTray.initSystemTray(
      title: "desktop-app",
      iconPath: path,
      toolTip: 'Open App',
    );

    // create context menu
    final Menu menu = Menu();
    await menu.buildFrom([
      // Add menus here

      MenuItemLabel(
        label: 'Hide',
      ),
    ]);

    // set context menu
    await systemTray.setContextMenu(menu);

    // handle system tray event
    systemTray.registerSystemTrayEventHandler((eventName) {
      debugPrint("eventName: $eventName");
      if (eventName == kSystemTrayEventClick) {
        Platform.isWindows ? appWindow.show() : systemTray.popUpContextMenu();
      } else if (eventName == kSystemTrayEventRightClick) {
        Platform.isWindows ? systemTray.popUpContextMenu() : appWindow.show();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: WindowBorder(
        color: Colors.grey,
        child: Column(
          children: const [
            WindowTitleBar(),
            Divider(
              color: Colors.grey,
              height: 1,
            ),
            HomePageListChat(),
          ],
        ),
      ),
    );
  }
}

class WindowTitleBar extends StatelessWidget {
  const WindowTitleBar({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: WindowTitleBarBox(
        child: Row(
          children: [
            appTitle("Green Chat"),
            Expanded(child: MoveWindow()),
            const WindowButtons(),
          ],
        ),
      ),
    );
  }

  Widget appTitle(title) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Row(
        children: [
          Container(
            height: 30,
            width: 30,
            margin: const EdgeInsets.only(right: 12),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
            child: const Image(
              image: NetworkImage(
                'https://media.istockphoto.com/id/1186954642/vector/chat-message-bubbles-icon-on-white-background-vector-stock-illustration.jpg?s=612x612&w=0&k=20&c=YUCfUA34DKg-ne9rYKXtdElJ6xy6N_Locz1ecC8-zZU=',
              ),
            ),
          ),
          Text(
            title,
            style: const TextStyle(color: Colors.black),
          ),
          MoveWindow()
        ],
      ),
    );
  }
}

class WindowButtons extends StatefulWidget {
  const WindowButtons({Key? key}) : super(key: key);

  @override
  State<WindowButtons> createState() => _WindowButtonsState();
}

class _WindowButtonsState extends State<WindowButtons> {
  void maximizeOrRestore() {
    setState(() {
      appWindow.maximizeOrRestore();
    });
  }

  void minimize() {
    setState(() {
      appWindow.minimize();
    });
  }

  void closeApp() {
    setState(() {
      appWindow.close();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        WindowButtonItems(
          onPressed: () {},
          icon: Icons.more_horiz,
        ),
        WindowButtonItems(
          onPressed: () {
            minimize();
          },
          icon: Icons.minimize,
        ),
        WindowButtonItems(
          onPressed: () {
            maximizeOrRestore();
          },
          icon: Icons.square_outlined,
        ),
        WindowButtonItems(
          onPressed: () {
            closeApp();
          },
          icon: Icons.close,
        ),
      ],
    );
  }
}

class WindowButtonItems extends StatefulWidget {
  final IconData icon;
  final Function() onPressed;
  const WindowButtonItems(
      {super.key, required this.icon, required this.onPressed});

  @override
  State<WindowButtonItems> createState() => _WindowButtonState();
}

class _WindowButtonState extends State<WindowButtonItems> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 35,
      width: 40,
      child: InkWell(
        onTap: widget.onPressed,
        child: Icon(
          widget.icon,
          size: 16,
          color: Colors.black,
        ),
      ),
    );
  }
}
