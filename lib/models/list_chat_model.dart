class ListChatModel {
  final String userName;
  final String imageLink;
  final String recentMessage;

  ListChatModel({
    required this.userName,
    required this.imageLink,
    required this.recentMessage,
  });

  factory ListChatModel.fromJson(Map<String, dynamic> json) {
    return ListChatModel(
      userName: json['userName'],
      imageLink: json['imageLink'],
      recentMessage: json['recentMessage'],
    );
  }
}
